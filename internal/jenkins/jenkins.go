package jenkins

import (
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"os/exec"
)

type Connection struct {
	URL      string
	Username string
	Password string
}

func NewConnection(url string, username string, password string) Connection {
	return Connection{
		URL:      url,
		Username: username,
		Password: password,
	}
}

// executes the given command against the jenkins instance
func (c *Connection) ExecuteCmd(command string) (string, error) {
	if len(command) == 0 {
		logrus.Error("Command is empty")
		return "", errors.New("command is empty")
	}
	cmd, err := exec.Command("java",
		"-jar", "/tmp/jenkins-cli.jar",
		"-s", c.URL,
		"-auth", fmt.Sprintf("%s:%s", c.Username, c.Password),
		command,
	).Output()

	if err != nil {
		logrus.Error("Could not execute command", err)
		return "", err
	}
	logrus.Info("Output from jenkins: ", string(cmd))

	return string(cmd), nil
}
