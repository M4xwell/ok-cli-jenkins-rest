package routes

import (
	"fmt"
	"net/http"
)

func Live() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		fmt.Fprint(w, "ok")
	}
}
