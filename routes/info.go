package routes

import (
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"jenkins-api/internal/jenkins"
	"net/http"
	"strings"
)

type Information struct {
	Version       string `json:"version"`
	Authenticated string `json:"authenticated"`
}

func Info() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// read request body
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			logrus.Warn("Could not read request body")
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprint(w)
			return
		}
		// marshal request body to struct
		var con jenkins.Connection
		if err = json.Unmarshal(body, &con); err != nil {
			logrus.Warn("Could not unmarshal request body")
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w)
			return
		}
		// collect info
		info := getInfoFromJenkins(con)
		// send response
		json.NewEncoder(w).Encode(info)
	}
}

// converts the info from the jenkins instance to an info object
func getInfoFromJenkins(con jenkins.Connection) Information {
	var info Information

	connection := jenkins.NewConnection(con.URL, con.Username, con.Password)

	info.Version, _ = connection.ExecuteCmd("version")
	// remove line feed from version string
	info.Version = info.Version[:len(info.Version)-1]

	out, _ := connection.ExecuteCmd("who-am-i")

	info.Authenticated = extractUser(out)

	return info
}

// extract the user info from the jenkins output
func extractUser(payload string) string {
	tmp := strings.Split(strings.TrimSuffix(payload, "\n"), "\n")[0]
	return tmp[18:]
}
