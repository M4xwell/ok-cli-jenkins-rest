package main

import (
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"jenkins-api/routes"
	"net/http"
)

func initRoutes() *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/health/live", routes.Live()).Methods("GET")
	r.HandleFunc("/health/info", routes.Info()).Methods("POST")

	return r
}
func main() {
	logrus.Info("Starting Jenkins API")
	router := initRoutes()

	if err := http.ListenAndServe(":8080", router); err != nil {
		logrus.Errorf("Could not serve, %+v", err)
	}
}
